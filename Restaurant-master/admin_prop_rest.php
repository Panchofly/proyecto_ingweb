<!DOCTYPE html>
<?php
session_start();
if (@!$_SESSION['user']) {
    header("Location:index.php");
}elseif ($_SESSION['rol']==2) {
    header("Location:index2.php");
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Restaurant</title>
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css" media="screen" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/style-portfolio.css">
        <link rel="stylesheet" href="css/picto-foundry-food.css" />
        <link rel="stylesheet" href="css/jquery-ui.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link rel="icon" href="favicon-1.ico" type="image/x-icon">
    </head>

    <body>

        <?php 
            include("static/menu4.php");
        ?>

        <section  id="reservation"  class="description_content">
            <div class="well well-small">
            <hr class="soft"/>
            <div class="jumbotron">
                <h2>ADMINISTRADOR DE RESTAURANTE REGISTRADO</h2>     
            </div>
            
            <div class="row-fluid">
                <?php

                    require("static/connect_db.php");
                    $hola2=$_SESSION['user'];

                    $sql=("SELECT * FROM registro WHERE user='$hola2'");
                    $sqll=mysqli_query($mysqli,$sql);
                    $check_user=mysqli_num_rows($sqll);
                    if($check_user>0){
                            extract($_GET);
                            $sql="SELECT id_registro FROM registro WHERE user='$hola2'";
                            $ressql=mysqli_query($mysqli,$sql);
                            while ($row=mysqli_fetch_row ($ressql)){
                                $id=$row[0];
                            }
                            $sql2="SELECT nombre_propietario FROM propietario WHERE id_registro='$id'";
                            $ressql2=mysqli_query($mysqli,$sql2);
                            while ($row=mysqli_fetch_row ($ressql2)){
                                $nomprop=$row[0];
                            }
                        }
                    $sql=("SELECT r.id_restaurante,p.nombre_propietario,r.nombre,r.representante,r.direccion,c.categoria, b.cantidad_mesas, re.user
                           FROM restaurantes r, propietario p, registro re,categorias c,bienes b
                           WHERE p.nombre_propietario = '$nomprop' and p.id_ruc=r.id_ruc and p.id_registro=re.id_registro and r.id_categoria= c.id_categoria and p.id_ruc = b.id_ruc");
        
    //la variable  $mysqli viene de connect_db que lo traigo con el require("connect_db.php");
                    $query=mysqli_query($mysqli,$sql);

                    echo "<table border='1'; class='table table-hover';>";
                        echo "<tr class='warning'>";
                            echo "<td>Id Restaurante</td>";
                            echo "<td>Nombre propietario</td>";
                            echo "<td>Nombre Restaurante</td>";
                            echo "<td>Nombre Representante</td>";
                            echo "<td>Dirección</td>";
                            echo "<td>Categoría</td>";
                            echo "<td>Cantidad mesas</td>";
                            echo "<td>Usuario</td>";
                            echo "<td>Editar</td>";
                        echo "</tr>";
                ?>
                <?php 
                     while($arreglo=mysqli_fetch_array($query)){
                        echo "<tr class='success'>";
                            echo "<td>$arreglo[0]</td>";
                            echo "<td>$arreglo[1]</td>";
                            echo "<td>$arreglo[2]</td>";
                            echo "<td>$arreglo[3]</td>";
                            echo "<td>$arreglo[4]</td>";
                            echo "<td>$arreglo[5]</td>";
                            echo "<td>$arreglo[6]</td>";
                            echo "<td>$arreglo[7]</td>";                            
                            echo "<td><a href='crud/actualizar_resta.php?id=$arreglo[0]'><img src='images/actualizar.gif' lass='img-rounded'></td>";
                           // echo "<td><a href='admin.php?id=$arreglo[0]&idborrar=2'><img src='images/eliminar.png' class='img-rounded'/></a></td>";   
                        echo "</tr>";
                    }

                    echo "</table>";

                        //extract($_GET);
                        //if(@$idborrar==2){
            //
                        //    $sqlborrar="DELETE FROM registro WHERE id_registro=$id";
                        //    $resborrar=mysqli_query($mysqli,$sqlborrar);
                        //    echo '<script>alert("REGISTRO ELIMINADO")</script> ';
                            //header('Location: proyectos.php');
                        //    echo "<script>location.href='admin.php'</script>";
                        //}

                ?>  
            </div>  
        </section>
       
        <?php 
            include("static/footer.php");
        ?>


        <script type="text/javascript" src="js/jquery-1.10.2.min.js"> </script>
        <script type="text/javascript" src="js/bootstrap.min.js" ></script>
        <script type="text/javascript" src="js/jquery-1.10.2.js"></script>     
        <script type="text/javascript" src="js/jquery.mixitup.min.js" ></script>
        <script type="text/javascript" src="js/main.js" ></script>

    </body>
</html>