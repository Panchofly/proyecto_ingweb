<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="row">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="../index.php">Restaurante</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav main-nav  clear navbar-right ">
                            <li><a class="navactive color_animation" href="#top">BIENVENIDO</a></li>
                            <li><a class="color_animation" href="#story">ACERCA</a></li>
                            <li><a class="color_animation" href="#pricing">PRECIOS</a></li>
                            <li><a class="color_animation" href="#beer">CERVEZA</a></li>
                            <li><a class="color_animation" href="#bread">PAN</a></li>
                            <li><a class="color_animation" href="#featured">DESTACADO</a></li>
                            <li><a class="color_animation" href="#reservation">RESERVA</a></li>
                            <li><a class="color_animation" href="#contact">CONTACTANOS</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
            </div><!-- /.container-fluid -->
        </nav>