<footer class="sub_footer">
    <div class="container">
        <div class="col-md-4"><p class="sub-footer-text text-center">&copy; Gestionando páginas Web desde el 2017, Hecho por <a href="">Jonathan y Luzón </a></p></div>
        <div class="col-md-4"><p class="sub-footer-text text-center">Regresar a <a href="#top">ARRIBA</a></p></div>
        <div class="col-md-4"><p class="sub-footer-text text-center">Construido con cuidado por<a href="#" target="_blank"> Nosotros</a></p></div>
    </div>
</footer>