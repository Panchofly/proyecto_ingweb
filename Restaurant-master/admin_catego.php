<!DOCTYPE html>
<?php
session_start();
if (@!$_SESSION['user']) {
    header("Location:index.php");
}elseif ($_SESSION['rol']==2) {
    header("Location:index2.php");
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Restaurant</title>
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css" media="screen" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/style-portfolio.css">
        <link rel="stylesheet" href="css/picto-foundry-food.css" />
        <link rel="stylesheet" href="css/jquery-ui.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link rel="icon" href="favicon-1.ico" type="image/x-icon">
    </head>
    <body>
        <?php 
            include("static/menu3.php");
        ?>
        <section  id="reservation"  class="description_content">
            <div class="well well-small">
            <hr class="soft"/>
            <div class="jumbotron">
                <h2>ADMINISTRADOR DE CATEGORIAS</h2>     
            </div>    

            <br>
            <div class="row-fluid">
                <form id="contact-us" method="post" action="">
                    <!-- Left Inputs -->
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-6 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-xs-4">
                                       
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-xs-4">
                                        <!-- Name -->
                                        <?php
                                            require("static/connect_db.php");
                                            $sql=("SELECT * FROM categorias");        
                                            //la variable  $mysqli viene de connect_db que lo traigo con el require("connect_db.php");
                                            $query=mysqli_query($mysqli,$sql);
                                            //echo "hola".$id_categoria;
                                            echo "<table border='1'; class='table table-hover'; >";
                                                echo "<tr class='warning'>";
                                                    echo "<td>Id Categoria</td>";
                                                    echo "<td>Categoria</td>";
                                                echo "</tr>";
                                        ?>  
                                        <?php 
                                            while($arreglo=mysqli_fetch_array($query)){
                                                echo "<tr class='success'>";
                                                    echo "<td>$arreglo[0]</td>";
                                                    echo "<td>$arreglo[1]</td>";
                                                echo "</tr>";
                                            }
                                            echo "</table>";
                                        ?> 
                                        <!--<button type="submit" id="submit" value="6" name="submit" class="text-center form-btn form-btn">AGREGAR</button>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Clear -->
                    <div class="clear"></div>
                </form>                  
            </div>  
        </section>
        <?php 
            include("static/footer.php");
        ?>
        <script type="text/javascript" src="js/jquery-1.10.2.min.js"> </script>
        <script type="text/javascript" src="js/bootstrap.min.js" ></script>
        <script type="text/javascript" src="js/jquery-1.10.2.js"></script>     
        <script type="text/javascript" src="js/jquery.mixitup.min.js" ></script>
        <script type="text/javascript" src="js/main.js" ></script>
    </body>
</html>
