<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <title>Restaurant</title>
        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css" media="screen" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/style-portfolio.css">
        <link rel="stylesheet" href="../css/picto-foundry-food.css" />
        <link rel="stylesheet" href="../css/jquery-ui.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link rel="icon" href="favicon-1.ico" type="image/x-icon">
    </head>
    <?php
        extract($_GET);
        require("../static/connect_db.php");
        $sql="SELECT * FROM bienes WHERE id_bienes=$id";
        //la variable  $mysqli viene de connect_db que lo traigo con el require("connect_db.php");
        $ressql=mysqli_query($mysqli,$sql);
        while ($row=mysqli_fetch_row ($ressql)){
            $id_bien=$row[0];
            $canti_mesas=$row[1];
            $id_ruc=$row[2];
        }
        $sql2="SELECT r.nombre FROM restaurantes r,bienes_restaurantes br WHERE br.id_bienes='$id' and br.id_restaurante=r.id_restaurante";
        $ressql2=mysqli_query($mysqli,$sql2);
        while ($row=mysqli_fetch_row ($ressql2)){
            $nomb=$row[0];
        }
    ?>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="row">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="../admin_restaurantes.php">Restaurantes Loja</a>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </nav>
        <section  id="reservation"  class="description_content">
            <div class="text-content container"> 
                <div class="inner contact">
                    <!-- Form Area -->
                    <div class="contact-form">
                        <!-- Form -->
                        <form id="contact-us" method="POST" action="ejecutar_actualizar.php">
                            <!-- Left Inputs -->
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12 col-md-8 col-xs-6">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <!-- Name -->
                                                <label for="text">Id Bienes</label>
                                                <input type="text" name="id" class="form" value= "<?php echo $id_bien?>" readonly="readonly">
                                                <label for="text">Nombre Restaurante</label>
                                                <input type="text" name="nom" class="form" value="<?php echo $nomb?>"> 
                                            </div>
                                            <div class="col-lg-4 col-md-4">
                                                <label for="text">Cantidad de Mesas</label>
                                                <input type="text" name="canti" class="form" value="<?php echo $canti_mesas?>">
                                                <label for="text">Ruc</label>
                                                <input type="text" name="id_ru" class="form" value="<?php echo $id_ruc?>" readonly="readonly">
                                            </div>
                                            <div class="col-xs-3 ">
                                                <!-- Send Button -->
                                                <br>
                                                <button type="submit" id="submit" value="5" name="submit" class="text-center form-btn form-btn">GUARDAR</button> 
                                                <!--<button type="submit" id="submit" name="submit" class="text-center form-btn form-btn">INGRESE</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Clear -->
                            <div class="clear"></div>
                        </form>
                    </div><!-- End Contact Form Area -->
                </div><!-- End Inner -->
            </div>
        </section>
        <?php 
            include("../static/footer.php");
        ?>


        <script type="text/javascript" src="../js/jquery-1.10.2.min.js"> </script>
        <script type="text/javascript" src="../js/bootstrap.min.js" ></script>
        <script type="text/javascript" src="../js/jquery-1.10.2.js"></script>     
        <script type="text/javascript" src="../js/jquery.mixitup.min.js" ></script>
        <script type="text/javascript" src="../js/main.js" ></script>
    </body>
</html>
