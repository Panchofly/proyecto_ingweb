<section  id="menu_ingrese"  class="description_content">
            <div class="featured background_content">
                <h1>Registro de Usuarios!</h1>
            </div>
            <div class="text-content container"> 
                <div class="inner contact">
                    <!-- Form Area -->
                    <div class="contact-form">
                        <!-- Form -->
                        <form id="contact-us" method="post" action="crud/registro_user.php">
                            <!-- Left Inputs -->
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-md-6 col-xs-12">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                                <!-- Name -->
                                                <input type="text" name="realname" class="form" placeholder="Ingresa tu nombre" />
                                                <input type="email" name="nick" required="required" class="form" placeholder="Ingrese Email" />
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                                <input type="password" name="pass" id="pass" class="form"  placeholder="Ingrese contraceña" />
                                                <input type="password" name="rpass" id="rpass" required="required" class="form" required placeholder="repite contraseña" />
                                            </div>
                                            <div class="col-xs-6 ">
                                                <!-- Send Button -->
                                                <button type="submit" id="submit" name="submit" class="text-center form-btn form-btn">REGISTRAR</button> 
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Clear -->
                            <div class="clear"></div>
                        </form>
                    </div><!-- End Contact Form Area -->
                </div><!-- End Inner -->
            </div>
        </section>
