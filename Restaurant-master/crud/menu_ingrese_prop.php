
        <section  id="menu_ingrese_prop"  class="description_content">
            <div class="featured background_content">
                <h1>Registro de Propietarios!</h1>
            </div>
            <div class="text-content container"> 
                <div class="inner contact">
                    <!-- Form Area -->
                    <div class="contact-form">
                        <!-- Form -->
                        <form id="contact-us" method="POST" action="crud/registro_prop.php">
                            <!-- Left Inputs -->
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12 col-md-8 col-xs-6">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-xs-4">
                                                <!-- Name -->
                                                <input type="number" min="1" name="ruc" class="form" placeholder="Ingresa su ruc" />
                                                <input type="number" min="1" name="cedula" class="form" placeholder="Ingresa su cédula" />
                                                <input type="text" name="nombreprop" class="form" placeholder="Ingresa nombre del Propietario" />
                                                <input type="tel" name="telefono" class="form" placeholder="Ingresa su telefono" />
                                                <input type="email" name="email" class="form" placeholder="Ingrese Email" />
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-xs-4">
                                                <input type="text" name="realname" class="form" placeholder="Ingresa nombre de usuario" />
                                                <input type="password" name="pass" id="pass" class="form" placeholder="Ingrese una contraseña" />
                                                <input type="password" name="rpass" id="rpass" required="required" class="form" required placeholder="Repita la contraseña" />
                                                <input type="text" name="nombreres" class="form" placeholder="Ingresa nombre de restaurante" />
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-xs-4">
                                                
                                                <input type="text" name="nombrerepre" class="form" placeholder="Ingresa nombre del Representante" />
                                                <input type="text" name="direccion" class="form" placeholder="Ingresa dirección del restaurante" />
                                                <select class="form" name="categoria" id="categoria">
                                                    <option value="1">Primera</option>
                                                    <option value="2">Segunda</option>
                                                    <option value="3">Tercera</option>
                                                    <option value="4">Cuarta</option>
                                                </select>
                                                <input type="number" min="1" name="cantmesas" class="form" placeholder="ingrese cantidad de mesas" />
                                            </div>
                                            <div class="col-xs-4 ">
                                                <!-- Send Button -->
                                                <button type="submit" id="submit" name="submit" class="text-center form-btn form-btn">REGISTRAR</button> 
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Clear -->
                            <div class="clear"></div>
                        </form>
                    </div><!-- End Contact Form Area -->
                </div><!-- End Inner -->
            </div>
        </section>
        

