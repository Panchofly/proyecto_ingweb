<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <title>Restaurant</title>
        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css" media="screen" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/style-portfolio.css">
        <link rel="stylesheet" href="../css/picto-foundry-food.css" />
        <link rel="stylesheet" href="../css/jquery-ui.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link rel="icon" href="favicon-1.ico" type="image/x-icon">
    </head>

    <?php
        extract($_GET);
        require("../static/connect_db.php");
        //$sql="SELECT * FROM registro WHERE id_registro=$id";
        $sql2="SELECT * FROM propietario WHERE nombre_propietario='$nomprop'";
        $ressql2=mysqli_query($mysqli,$sql2);
        $sql3="SELECT re.user FROM registro re, propietario p WHERE p.nombre_propietario='$nomprop' and p.id_registro=re.id_registro";
        //la variable  $mysqli viene de connect_db que lo traigo con el require("connect_db.php");
        
        $ressql3=mysqli_query($mysqli,$sql3);
        //echo $ressql3;
        while ($row=mysqli_fetch_row ($ressql2)){
            $id_ruc=$row[0];
            $cedula=$row[1];
            $nombre_propietario=$row[2];
            $telefono=$row[3];
            $correo=$row[4];
            if ($row2=mysqli_fetch_row ($ressql3)) {
                $usera=$row2[0];
            }
        }
    ?>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="row">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="../index.php">Restaurantes Loja</a>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </nav>
        <section  id="reservation"  class="description_content">
            <div class="text-content container"> 
                <div class="inner contact">
                    <!-- Form Area -->
                    <div class="contact-form">
                        <!-- Form -->
                        <form id="contact-us" method="post" action="ejecutar_actualizar.php">
                            <!-- Left Inputs -->
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-md-6 col-xs-12">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-xs-4">
                                                <!-- Name -->                                                
                                                <input type="text" name="id2" class="form" value= "<?php echo $id_ruc;?>" readonly="readonly">
                                                <input type="text" name="cedu" class="form" value="<?php echo $cedula;?>">
                                                <input type="text" class="form" name="nomprop" value="<?php echo $nombre_propietario;?>">
                                                
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-xs-4 ">
                                                <input type="text" name="telefon" class="form" value="<?php echo $telefono;?>">
                                                <input type="text" class="form" name="email" value="<?php echo $correo;?>">
                                                <input type="text" class="form" name="user" value="<?php echo $usera;?>" readonly="readonly">
                                            </div>
                                            <div class="col-xs-4">
                                                <!-- Send Button -->
                                                <button type="submit" id="submit" value="2" name="submit" class="text-center form-btn form-btn">GUARDAR</button> 
                                                <!--<button type="submit" id="submit" name="submit" class="text-center form-btn form-btn">INGRESE</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Clear -->
                            <div class="clear"></div>
                        </form>
                    </div><!-- End Contact Form Area -->
                </div><!-- End Inner -->
            </div>
        </section>
        <?php 
            include("../static/footer.php");
        ?>


        <script type="text/javascript" src="../js/jquery-1.10.2.min.js"> </script>
        <script type="text/javascript" src="../js/bootstrap.min.js" ></script>
        <script type="text/javascript" src="../js/jquery-1.10.2.js"></script>     
        <script type="text/javascript" src="../js/jquery.mixitup.min.js" ></script>
        <script type="text/javascript" src="../js/main.js" ></script>
    </body>
</html>
