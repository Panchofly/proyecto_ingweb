<!DOCTYPE html>
<?php
session_start();
if (@!$_SESSION['user']) {
    header("Location:index.php");
}elseif ($_SESSION['rol']==2) {
    header("Location:index2.php");
}
?>
<html>

    <head>
        <meta charset="UTF-8">
        <title>Restaurant</title>
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css" media="screen" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/style-portfolio.css">
        <link rel="stylesheet" href="css/picto-foundry-food.css" />
        <link rel="stylesheet" href="css/jquery-ui.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link rel="icon" href="favicon-1.ico" type="image/x-icon">
    </head>
    <body>
        <?php 
            include("static/menu3.php");
        ?>
        <section  id="reservation"  class="description_content">
            <div class="well well-small">
            <hr class="soft"/>
            <div class="jumbotron">
                <h2>ADMINISTRADOR DE RESTAURANTES REGISTRADOS</h2>     
            </div>
            
            <div class="row-fluid">
                <?php

                    require("static/connect_db.php");
                    $sql=("SELECT * FROM restaurantes");
                            
    //la variable  $mysqli viene de connect_db que lo traigo con el require("connect_db.php");
                    $query=mysqli_query($mysqli,$sql);
                    //echo "hola".$id_categoria;
                    $sql2=("SELECT categoria FROM categorias c, restaurantes r WHERE c.id_categoria=r.id_categoria ORDER BY r.id_restaurante");
                    $query2=mysqli_query($mysqli,$sql2);

                    echo "<table border='1'; class='table table-hover';>";
                        echo "<tr class='warning'>";
                            echo "<td>Id Restaurante</td>";
                            echo "<td>Nombre Restaurante</td>";
                            echo "<td>Representante</td>";
                            echo "<td>Dirección</td>";
                            echo "<td>Ruc</td>";
                            echo "<td>Categoría</td>";
                            echo "<td>Editar</td>";
                            echo "<td>Borrar</td>";
                        echo "</tr>";
                ?>  
                <?php 
                    while($arreglo=mysqli_fetch_array($query)){
                        echo "<tr class='success'>";
                            echo "<td>$arreglo[0]</td>";
                            echo "<td>$arreglo[1]</td>";
                            echo "<td>$arreglo[2]</td>";
                            echo "<td>$arreglo[3]</td>";
                            echo "<td>$arreglo[4]</td>";
                        if ($arreglo2=mysqli_fetch_array($query2)){
                            echo "<td>$arreglo2[0]</td>";
                        }
                            echo "<td><a href='crud/actualizar_admin_rest.php?id=$arreglo[0]'><img src='images/actualizar.gif' class='img-rounded'></td>";
                            echo "<td><a href='admin_restaurantes.php?id=$arreglo[0]&idruc=$arreglo[4]'><img src='images/eliminar.png' class='img-rounded'/></a></td>";    
                        echo "</tr>";
                    }
                    echo "</table>";
                        extract($_GET);
                        $idruc2=@$idruc;
                        $sentencia="SELECT id_registro FROM propietario WHERE id_registro= id_registro and id_ruc='$idruc2'";
                        $query3=mysqli_query($mysqli,$sentencia);
                        while ($row=mysqli_fetch_row($query3)) {
                            $id_regi=$row[0];
                        }
                        $check_registro=mysqli_num_rows($query3);
                        if($check_registro>0){
                            $sqlborrar="DELETE FROM registro WHERE id_registro=$id_regi";
                            $resborrar=mysqli_query($mysqli,$sqlborrar);
                            echo '<script>alert("REGISTRO ELIMINADO")</script> ';
                            echo "<script>location.href='admin_restaurantes.php'</script>";
                        }
                ?>  
            </div>  
        </section>
        <?php 
            include("static/footer.php");
        ?>
        <script type="text/javascript" src="js/jquery-1.10.2.min.js"> </script>
        <script type="text/javascript" src="js/bootstrap.min.js" ></script>
        <script type="text/javascript" src="js/jquery-1.10.2.js"></script>     
        <script type="text/javascript" src="js/jquery.mixitup.min.js" ></script>
        <script type="text/javascript" src="js/main.js" ></script>
    </body>
</html>